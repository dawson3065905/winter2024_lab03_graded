import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		//animal array
		Seals[] pod = new Seals[4];
		
		//some temp fields for storage
		String userSize;
		int userWhiskerLength;
		int userFin;
		boolean userIsTropical;
		
		Scanner inputer = new Scanner(System.in);
		
		for(int i = 0; i < pod.length; i++){
			
			//asks user to fill out fields
			System.out.println("What is the size of your seal?");
				userSize = inputer.next();
				
			System.out.println("What is the whisker length (in cm) of your seal?");
				userWhiskerLength = inputer.nextInt();
				
			System.out.println("What is the fin length (in cm) of your seal?");
				userFin = inputer.nextInt();
				
			System.out.println("Is your seal tropical? (Input 'true' or 'false')");
			userIsTropical = inputer.nextBoolean();
			
			//seperates print spaces to make it clear to user they are inputing a new seal
			System.out.println("");
			
			//temp array to store data
			//overwritten each iteration
			Seals temp = new Seals(userSize, userWhiskerLength, userFin, userIsTropical);
			
			//stores into array using i
			pod[i] = temp;
		}
		
		//prints fields of last seal
		System.out.println("Field of last seal: " + 
		pod[pod.length-1].size + " " + 
		pod[pod.length-1].whiskerLength + " " +
		pod[pod.length-1].fin + " " +
		pod[pod.length-1].isTropical + "\r\n");
		
		//runs of typeOfSeal on first index
		System.out.println("First Seal: " + "\r\n" + 
		pod[0].typeOfSeal() + "\r\n");
		
		//runs of catchFish on second index
		System.out.println("Second Seal: " + "\r\n" + 
		pod[1].catchFish() + "\r\n");
	}
}